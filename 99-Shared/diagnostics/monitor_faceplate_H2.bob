<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2025-03-04 16:30:22 by lucianocarneiro-->
<display version="2.0.0">
  <name>ODH-H2Mon-0$(N_H2=01)</name>
  <width>360</width>
  <height>400</height>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <width>360</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color red="218" green="110" blue="0">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>H2 Monitor</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>340</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Status HCB</name>
    <x>20</x>
    <y>70</y>
    <width>320</width>
    <height>310</height>
    <line_width>0</line_width>
    <background_color>
      <color name="Group Background" red="200" green="205" blue="201">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Status HCB_1</name>
    <text>ODH-H2Mon-0$(N_H2=01)</text>
    <x>50</x>
    <y>70</y>
    <width>260</width>
    <height>30</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>331</y>
    <width>250</width>
    <height>25</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_45</name>
      <text>Hydrogen Level:</text>
      <width>120</width>
      <height>25</height>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="textupdate" version="2.0.0">
      <name>Text Update_1</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):H2level</pv_name>
      <x>150</x>
      <height>25</height>
      <rules>
        <rule name="New Rule" prop_id="precision" out_exp="false">
          <exp bool_exp="pv0&lt;1">
            <value>0</value>
          </exp>
          <exp bool_exp="pv0&gt;1">
            <value>-1</value>
          </exp>
          <pv_name>$(pv_name)</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Not_Valid_label</name>
      <text>NOT VALID</text>
      <x>150</x>
      <height>25</height>
      <visible>false</visible>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="15.0">
        </font>
      </font>
      <background_color>
        <color name="ATTENTION" red="252" green="242" blue="17">
        </color>
      </background_color>
      <transparent>false</transparent>
      <vertical_alignment>1</vertical_alignment>
      <rules>
        <rule name="New Rule" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0">
            <value>true</value>
          </exp>
          <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):AiBadValue</pv_name>
        </rule>
      </rules>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>110</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_37</name>
      <text>1ˢᵗ Stage H2 Alarm</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="multi_state_led" version="2.0.0">
      <name>ODH Alarm</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):H2Above10</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MAJOR ERROR" red="252" green="13" blue="27">
            </color>
          </color>
        </state>
      </states>
      <fallback_label>ODH Alarm Invalid</fallback_label>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>140</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_38</name>
      <text>2ⁿᵈ Stage H2 Alarm</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="multi_state_led" version="2.0.0">
      <name>ODH Alarm_1</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):H2Above25</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MAJOR ERROR" red="252" green="13" blue="27">
            </color>
          </color>
        </state>
      </states>
      <fallback_label>ODH Alarm Invalid</fallback_label>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>170</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_39</name>
      <text>H2 above 100% LEL</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="multi_state_led" version="2.0.0">
      <name>Confirmed ODH Alarm</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):H2Above100</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MAJOR ERROR" red="252" green="13" blue="27">
            </color>
          </color>
        </state>
      </states>
      <fallback_label>Confirmed ODH Alarm</fallback_label>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>200</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_40</name>
      <text>Special State</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="multi_state_led" version="2.0.0">
      <name>Confirmed ODH Alarm_1</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):SpecialState</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MAJOR ERROR" red="252" green="13" blue="27">
            </color>
          </color>
        </state>
      </states>
      <fallback_label>Confirmed ODH Alarm</fallback_label>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>260</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_43</name>
      <text>Limit Relay not triggered</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="multi_state_led" version="2.0.0">
      <name>HCB Low Battery Alarm</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):LimRelayFailure</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MINOR ERROR" red="255" green="235" blue="17">
            </color>
          </color>
        </state>
      </states>
      <fallback_label></fallback_label>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>60</x>
    <y>290</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="multi_state_led" version="2.0.0">
      <name>HCB Audible Alarm Muted</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):LimRelaysDiscrepancy</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MINOR ERROR" red="255" green="235" blue="17">
            </color>
          </color>
        </state>
      </states>
      <fallback_label></fallback_label>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Label_44</name>
      <text>Wrong combination of relays</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_1</name>
    <x>60</x>
    <y>230</y>
    <width>240</width>
    <height>20</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="label" version="2.0.0">
      <name>Label_46</name>
      <text>FDI error</text>
      <x>30</x>
      <width>210</width>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="multi_state_led" version="2.0.0">
      <name>HCB Low Battery Alarm_1</name>
      <pv_name>$(P)ODH-H2Mon-$(N_OLD=0)$(N_H2=01):FdiBadValue</pv_name>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <states>
        <state>
          <value>0</value>
          <label></label>
          <color>
            <color name="OFF" red="90" green="110" blue="90">
            </color>
          </color>
        </state>
        <state>
          <value>1</value>
          <label></label>
          <color>
            <color name="MINOR ERROR" red="255" green="235" blue="17">
            </color>
          </color>
        </state>
      </states>
      <fallback_label></fallback_label>
    </widget>
  </widget>
</display>
