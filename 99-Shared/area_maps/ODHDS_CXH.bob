<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-10-23 10:34:24 by luciano-->
<display version="2.0.0">
  <name>ODHDS CXH</name>
  <macros>
    <P>CXB-CXH:ODH-Area</P>
    <N_OLD></N_OLD>
  </macros>
  <width>2550</width>
  <height>1290</height>
  <grid_visible>false</grid_visible>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <width>2550</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color red="218" green="110" blue="0">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>CB ODHDS - CXH</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>1180</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>PLC HW Failure_1</name>
    <pv_name>$(P):HwError</pv_name>
    <x>1240</x>
    <y>72</y>
    <width>120</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <square>true</square>
    <rules>
      <rule name="New Rule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0!=1">
          <value>
            <color name="WHITE" red="255" green="255" blue="255">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>
            <color name="TEXT" red="25" green="25" blue="25">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>PLC HW
Failure</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>PLC HW
Failure</label>
        <color>
          <color name="MINOR ERROR" red="255" green="235" blue="17">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>PLC HW
Failure</fallback_label>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Trend_1</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../popups/ODHACC_Trend.bob</file>
        <macros>
          <PLT>ODH_Trend_CXB.plt</PLT>
        </macros>
        <target>window</target>
      </action>
    </actions>
    <text>Open Trend</text>
    <x>2315</x>
    <y>72</y>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Diagnostics_1</name>
    <actions>
      <action type="open_display">
        <description>Open Display</description>
        <file>../diagnostics/CB_ODHDS_Diagnostics.bob</file>
        <target>tab</target>
      </action>
    </actions>
    <text>Diagnostics</text>
    <x>2430</x>
    <y>72</y>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Status:_1</name>
    <text>Status:</text>
    <x>20</x>
    <y>72</y>
    <width>80</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="21.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>ODH Alarm_1</name>
    <pv_name>$(P):Alrm</pv_name>
    <x>110</x>
    <y>72</y>
    <width>120</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <square>true</square>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>ODH Alarm</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>ODH Alarm</label>
        <color>
          <color name="MAJOR ERROR" red="252" green="13" blue="27">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>ODH Alarm Invalid</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>Confirmed ODH Alarm_2</name>
    <pv_name>$(P):ConfirmedAlrm</pv_name>
    <x>240</x>
    <y>72</y>
    <width>140</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <square>true</square>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>Confirmed ODH Alarm</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>Confirmed ODH Alarm</label>
        <color>
          <color name="MAJOR ERROR" red="252" green="13" blue="27">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>Confirmed ODH Alarm</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>ODH Warning_1</name>
    <pv_name>$(P):Warning</pv_name>
    <x>390</x>
    <y>72</y>
    <width>120</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <square>true</square>
    <rules>
      <rule name="New Rule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0!=1">
          <value>
            <color name="WHITE" red="255" green="255" blue="255">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>
            <color name="TEXT" red="25" green="25" blue="25">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>ODH Warning</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>ODH Warning</label>
        <color>
          <color name="MINOR ERROR" red="255" green="235" blue="17">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>ODH Warning</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>Confirmed ODH Alarm_3</name>
    <pv_name>$(P):Error</pv_name>
    <x>520</x>
    <y>72</y>
    <width>140</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <square>true</square>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>Monitor Error</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>Monitor Error</label>
        <color>
          <color name="MAJOR ERROR" red="252" green="13" blue="27">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>Monitor Error</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>Warming-Up_1</name>
    <pv_name>$(P):WarmingUp</pv_name>
    <x>670</x>
    <y>72</y>
    <width>130</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <square>true</square>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>Warming-Up</label>
        <color>
          <color red="114" green="115" blue="115">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>Warming-Up</label>
        <color>
          <color name="MAJOR ERROR" red="252" green="13" blue="27">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>Warming-Up</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>Maintenance_1</name>
    <pv_name>$(P):Maintenance</pv_name>
    <x>810</x>
    <y>72</y>
    <width>120</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <square>true</square>
    <rules>
      <rule name="New Rule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0!=1">
          <value>
            <color name="WHITE" red="255" green="255" blue="255">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>
            <color name="TEXT" red="25" green="25" blue="25">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>Maintenance</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>Maintenance</label>
        <color>
          <color name="MASKED" red="254" green="194" blue="81">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>Maintenance</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>Audible Alarm Muted_1</name>
    <pv_name>$(P):Muted</pv_name>
    <x>940</x>
    <y>72</y>
    <width>140</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <square>true</square>
    <rules>
      <rule name="New Rule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0!=1">
          <value>
            <color name="WHITE" red="255" green="255" blue="255">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>
            <color name="TEXT" red="25" green="25" blue="25">
            </color>
          </value>
        </exp>
        <pv_name>$(pv_name)</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>Audible Alarm
Muted</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>Audible Alarm
Muted</label>
        <color>
          <color name="MINOR ERROR" red="255" green="235" blue="17">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>Audible Alarm
Muted</fallback_label>
  </widget>
  <widget type="multi_state_led" version="2.0.0">
    <name>ODH AlarmReset Required_1</name>
    <pv_name>$(P):ResetRequired</pv_name>
    <x>1090</x>
    <y>72</y>
    <width>140</width>
    <height>60</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <square>true</square>
    <rules>
      <rule name="New Rule" prop_id="foreground_color" out_exp="false">
        <exp bool_exp="pv0==0">
          <value>
            <color name="WHITE" red="255" green="255" blue="255">
            </color>
          </value>
        </exp>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <states>
      <state>
        <value>0</value>
        <label>Reset Required</label>
        <color>
          <color red="112" green="115" blue="114">
          </color>
        </color>
      </state>
      <state>
        <value>1</value>
        <label>Reset Required</label>
        <color>
          <color name="MAJOR ERROR" red="252" green="13" blue="27">
          </color>
        </color>
      </state>
    </states>
    <fallback_label>Reset Required</fallback_label>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <macros>
      <P>CXB-CXH:</P>
    </macros>
    <x>20</x>
    <y>155</y>
    <width>2530</width>
    <height>1114</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Details</name>
      <width>2520</width>
      <height>1114</height>
      <line_width>0</line_width>
      <background_color>
        <color name="Group Background" red="200" green="205" blue="201">
        </color>
      </background_color>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>ODHDS_Area_Map_CXH_Controled-Area</name>
      <file>../symbols/ODHDS_Area_Map_CXH/ODHDS_Area_Map_CXH_Controled-Area_v02.svg</file>
      <x>10</x>
      <y>205</y>
      <width>2490</width>
      <height>587</height>
      <stretch_image>true</stretch_image>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>CXH red + violet</name>
      <pv_name>=`CXB-CXH:ODH-Area:State`/10</pv_name>
      <symbols>
        <symbol></symbol>
        <symbol></symbol>
        <symbol></symbol>
        <symbol></symbol>
        <symbol>../symbols/ODHDS_Area_Map_CXH/ODHDS_Area_Map_CXH_violet_v02.svg</symbol>
        <symbol>../symbols/ODHDS_Area_Map_CXH/ODHDS_Area_Map_CXH_red_v02.svg</symbol>
        <symbol>../symbols/ODHDS_Area_Map_CXH/ODHDS_Area_Map_CXH_red_v02.svg</symbol>
      </symbols>
      <x>10</x>
      <y>205</y>
      <width>2490</width>
      <height>587</height>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <fallback_symbol>../symbols/ODHDS_Area_Map_CXH/ODHDS_Area_Map_CXH_violet_v02.svg</fallback_symbol>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>ODHDS_Area_Map_CXH</name>
      <file>../symbols/ODHDS_Area_Map_CXH/ODHDS_Area_Map_CXH_v02.svg</file>
      <x>10</x>
      <y>205</y>
      <width>2490</width>
      <height>587</height>
      <stretch_image>true</stretch_image>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>ODHDS_CXH_embedded_ODH_Monitors</name>
      <file>embedded_details/ODHDS_CXH_embedded_ODH_Monitors.bob</file>
      <width>2510</width>
      <height>1114</height>
      <resize>2</resize>
      <rules>
        <rule name="New Rule" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0">
            <value>false</value>
          </exp>
          <pv_name>loc://Devices</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>ODHDS_CXH_embedded_ODH_Devices</name>
      <file>embedded_details/ODHDS_CXH_embedded_ODH_Devices.bob</file>
      <width>2520</width>
      <height>1114</height>
      <visible>false</visible>
      <resize>2</resize>
      <rules>
        <rule name="New Rule" prop_id="visible" out_exp="false">
          <exp bool_exp="pv0">
            <value>true</value>
          </exp>
          <pv_name>loc://Devices</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Area Map GO2 CXH</name>
      <text>Coldbox Hall</text>
      <x>40</x>
      <width>320</width>
      <height>85</height>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="21.0">
        </font>
      </font>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Legend</name>
      <actions>
        <action type="open_display">
          <description>Open Display</description>
          <file>../area_maps/Area_Map_Legend.bob</file>
          <target>window</target>
        </action>
      </actions>
      <text>Legend</text>
      <x>2380</x>
      <y>1065</y>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>ODH Monitor_2</name>
      <actions>
      </actions>
      <text></text>
      <x>2250</x>
      <y>15</y>
      <width>120</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="label" version="2.0.0">
      <name>ODH Monitors_1</name>
      <text>ODH Monitors</text>
      <x>2250</x>
      <y>50</y>
      <width>120</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>ODH_Symbol_1</name>
      <symbols>
        <symbol>../symbols/Button_ODH_and_Alarm-Device_Symbol_v01/Button_ODH_Symbol.svg</symbol>
      </symbols>
      <x>2293</x>
      <y>19</y>
      <width>33</width>
      <height>33</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>ODH Monitor_3</name>
      <actions execute_as_one="true">
        <action type="write_pv">
          <description>WritePV</description>
          <pv_name>loc://O2Mon</pv_name>
          <value>1</value>
        </action>
        <action type="write_pv">
          <description>WritePV</description>
          <pv_name>loc://Devices</pv_name>
          <value>0</value>
        </action>
      </actions>
      <text></text>
      <x>2250</x>
      <y>15</y>
      <width>120</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <transparent>true</transparent>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Alarm-Device (only for Visual)_1</name>
      <actions>
      </actions>
      <text></text>
      <x>2386</x>
      <y>15</y>
      <width>120</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="symbol" version="2.0.0">
      <name>Alarm-Device_Symbol._1</name>
      <symbols>
        <symbol>../symbols/Button_ODH_and_Alarm-Device_Symbol_v01/Button_Alarm-Device_Symbol.svg</symbol>
      </symbols>
      <x>2426</x>
      <y>19</y>
      <width>32</width>
      <height>32</height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>Alarm Devices_1</name>
      <text>Alarm Devices</text>
      <x>2386</x>
      <y>50</y>
      <width>120</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Alarm-Device_1</name>
      <actions execute_as_one="true">
        <action type="write_pv">
          <description>WritePV</description>
          <pv_name>loc://Devices</pv_name>
          <value>1</value>
        </action>
        <action type="write_pv">
          <description>WritePV</description>
          <pv_name>loc://O2Mon</pv_name>
          <value>0</value>
        </action>
      </actions>
      <text></text>
      <x>2386</x>
      <y>15</y>
      <width>120</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <transparent>true</transparent>
      <tooltip>$(actions)</tooltip>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>PSS Overview</name>
      <actions>
        <action type="open_display">
          <description>Open Display</description>
          <file>../../10-Top/overview.bob</file>
          <target>replace</target>
        </action>
      </actions>
      <text>GDS Overview</text>
      <x>2250</x>
      <y>1065</y>
      <width>120</width>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
        </font>
      </font>
      <tooltip>$(actions)</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group_1</name>
    <macros>
      <P_AREA>CXB-CXH:</P_AREA>
    </macros>
    <x>1380</x>
    <y>72</y>
    <width>250</width>
    <height>60</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="action_button" version="3.0.0">
      <name>Acknowledge Alarm</name>
      <actions>
        <action type="write_pv">
          <description>WritePV</description>
          <pv_name>$(pv_name)</pv_name>
          <value>1</value>
        </action>
      </actions>
      <pv_name>$(P_AREA):ZoneCmd_ResetAlarm</pv_name>
      <text>Reset</text>
      <width>120</width>
      <height>60</height>
      <font>
        <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
        </font>
      </font>
      <rules>
        <rule name="New Rule" prop_id="enabled" out_exp="false">
          <exp bool_exp="pv0">
            <value>true</value>
          </exp>
          <pv_name>$(P):ResetRequired</pv_name>
        </rule>
      </rules>
      <tooltip>$(pv_name)
$(pv_value)</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <enabled>false</enabled>
    </widget>
    <widget type="group" version="3.0.0">
      <name>Group_2</name>
      <x>130</x>
      <width>120</width>
      <height>60</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="action_button" version="3.0.0">
        <name>Unmute Alarm Visual</name>
        <text></text>
        <width>120</width>
        <height>60</height>
        <font>
          <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
          </font>
        </font>
        <rules>
          <rule name="New Rule" prop_id="enabled" out_exp="false">
            <exp bool_exp="pv0">
              <value>true</value>
            </exp>
            <pv_name>$(P):MuteAllowed</pv_name>
          </rule>
        </rules>
        <tooltip>$(pv_value)</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
        <enabled>false</enabled>
      </widget>
      <widget type="textupdate" version="2.0.0">
        <name>Text Update</name>
        <pv_name>$(P):MuteTimeLeft</pv_name>
        <x>13</x>
        <y>35</y>
        <visible>false</visible>
        <font>
          <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
          </font>
        </font>
        <transparent>true</transparent>
        <horizontal_alignment>1</horizontal_alignment>
        <rules>
          <rule name="New Rule" prop_id="visible" out_exp="false">
            <exp bool_exp="pv0 and pv1">
              <value>true</value>
            </exp>
            <pv_name>$(P):Muted</pv_name>
            <pv_name>$(P):MuteAllowed</pv_name>
          </rule>
        </rules>
        <border_alarm_sensitive>false</border_alarm_sensitive>
      </widget>
      <widget type="action_button" version="3.0.0">
        <name>Unmute Alarm</name>
        <actions>
          <action type="write_pv">
            <description>WritePV</description>
            <pv_name>$(pv_name)</pv_name>
            <value>1</value>
          </action>
        </actions>
        <pv_name>$(P_AREA):ZoneCmd_UnmuteAlarm</pv_name>
        <text>Unmute Alarm
 </text>
        <width>120</width>
        <height>60</height>
        <visible>false</visible>
        <font>
          <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
          </font>
        </font>
        <transparent>true</transparent>
        <rules>
          <rule name="Visible" prop_id="visible" out_exp="false">
            <exp bool_exp="pv0">
              <value>true</value>
            </exp>
            <pv_name>$(P):Muted</pv_name>
          </rule>
          <rule name="Allowed" prop_id="enabled" out_exp="false">
            <exp bool_exp="pv0">
              <value>true</value>
            </exp>
            <pv_name>$(P):MuteAllowed</pv_name>
          </rule>
        </rules>
        <tooltip>$(pv_name)
$(pv_value)</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
        <enabled>false</enabled>
      </widget>
      <widget type="action_button" version="3.0.0">
        <name>Mute Alarm</name>
        <actions>
          <action type="write_pv">
            <description>WritePV</description>
            <pv_name>$(pv_name)</pv_name>
            <value>1</value>
          </action>
        </actions>
        <pv_name>$(P_AREA):ZoneCmd_MuteAlarm</pv_name>
        <text>Mute Alarm</text>
        <width>120</width>
        <height>60</height>
        <font>
          <font family="Source Sans Pro Semibold" style="REGULAR" size="16.0">
          </font>
        </font>
        <rules>
          <rule name="Allowed" prop_id="enabled" out_exp="false">
            <exp bool_exp="pv0">
              <value>true</value>
            </exp>
            <pv_name>$(P):MuteAllowed</pv_name>
          </rule>
          <rule name="Visible" prop_id="visible" out_exp="false">
            <exp bool_exp="pv0">
              <value>false</value>
            </exp>
            <pv_name>$(P):Muted</pv_name>
          </rule>
        </rules>
        <tooltip>$(pv_name)
$(pv_value)</tooltip>
        <border_alarm_sensitive>false</border_alarm_sensitive>
        <enabled>false</enabled>
      </widget>
    </widget>
  </widget>
</display>
