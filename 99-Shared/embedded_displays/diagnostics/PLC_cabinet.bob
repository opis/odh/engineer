<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2023-09-14 11:49:16 by lucianocarneiro-->
<display version="2.0.0">
  <name>PSS1 PSS PLC Cabinet</name>
  <width>240</width>
  <height>350</height>
  <widget type="group" version="2.0.0">
    <name>Cabinet</name>
    <width>240</width>
    <height>350</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>BGGrey03-background</name>
      <width>240</width>
      <height>350</height>
      <line_width>2</line_width>
      <line_color>
        <color name="RED-GROUP-BORDER" red="167" green="134" blue="130">
        </color>
      </line_color>
      <background_color>
        <color name="RED-GROUP-BACKGROUND" red="208" green="175" blue="172">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
      <rules>
        <rule name="Background color" prop_id="background_color" out_exp="false">
          <exp bool_exp="pvInt0">
            <value>
              <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
              </color>
            </value>
          </exp>
          <pv_name>$(P):Cabinet_Stat</pv_name>
        </rule>
        <rule name="Line color" prop_id="line_color" out_exp="false">
          <exp bool_exp="pvInt0">
            <value>
              <color name="GROUP-BORDER" red="150" green="155" blue="151">
              </color>
            </value>
          </exp>
          <pv_name>$(P):Cabinet_Stat</pv_name>
        </rule>
      </rules>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button</name>
      <actions>
        <action type="open_display">
          <file>$(BOB)</file>
          <target>window</target>
          <description>Open Display</description>
        </action>
      </actions>
      <pv_name>$(P):CabHealthy</pv_name>
      <text>PLC Cabinet</text>
      <x>10</x>
      <y>5</y>
      <width>220</width>
      <font>
        <font family="Source Sans Pro" style="BOLD" size="18.0">
        </font>
      </font>
      <background_color>
        <color name="ERROR" red="252" green="13" blue="27">
        </color>
      </background_color>
      <rules>
        <rule name="Background" prop_id="background_color" out_exp="false">
          <exp bool_exp="pv0==1">
            <value>
              <color name="Button_Background" red="236" green="236" blue="236">
              </color>
            </value>
          </exp>
          <pv_name>$(P):CabHealthy</pv_name>
        </rule>
      </rules>
      <tooltip>$(pv_name)
$(pv_value)</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
      <enabled>$(CABINET_BUTTON=true)</enabled>
    </widget>
    <widget type="label" version="2.0.0">
      <name>FBS</name>
      <text>$(FBS==ESS.ACC.F05.U01)</text>
      <x>10</x>
      <y>35</y>
      <width>220</width>
      <font>
        <font name="Fine Print" family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <horizontal_alignment>1</horizontal_alignment>
      <wrap_words>false</wrap_words>
    </widget>
    <widget type="group" version="2.0.0">
      <name>Signals</name>
      <x>10</x>
      <y>55</y>
      <width>220</width>
      <height>285</height>
      <style>3</style>
      <transparent>true</transparent>
      <widget type="rectangle" version="2.0.0">
        <name>Rectangle</name>
        <width>220</width>
        <height>285</height>
        <line_width>2</line_width>
        <line_color>
          <color name="GROUP-BORDER" red="150" green="155" blue="151">
          </color>
        </line_color>
        <background_color>
          <color name="Background" red="220" green="225" blue="221">
          </color>
        </background_color>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_SM_Stat</name>
        <pv_name>$(P):ElDiag_SelModHealthy</pv_name>
        <x>15</x>
        <y>10</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_SM_Stat</name>
        <text>Selectivity Module</text>
        <x>41</x>
        <y>10</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_SCL_Stat</name>
        <pv_name>$(P):ElDiag_NtSwHealthy</pv_name>
        <x>15</x>
        <y>40</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_SCL_Stat</name>
        <text>Scalance Switch</text>
        <x>41</x>
        <y>40</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSAlrm</name>
        <pv_name>$(P):ElDiag_UpsAlarm</pv_name>
        <x>15</x>
        <y>70</y>
        <off_color>
          <color name="LED-RED-OFF" red="110" green="101" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSAlarm</name>
        <text>UPS Alarm</text>
        <x>41</x>
        <y>70</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_SurgeArrOK</name>
        <pv_name>$(P):ElDiag_SurProt-Ok</pv_name>
        <x>15</x>
        <y>100</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_SurgeArrOK</name>
        <text>Surge Arrester OK</text>
        <x>41</x>
        <y>100</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_PS24vOK</name>
        <pv_name>$(P):ElDiag_Psu24vHealthy</pv_name>
        <x>15</x>
        <y>130</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_PS24vOK</name>
        <text>Power Supply OK</text>
        <x>41</x>
        <y>130</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSBatOK</name>
        <pv_name>$(P):ElDiag_SplyFromBat</pv_name>
        <x>15</x>
        <y>160</y>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSBatOK</name>
        <text>UPS 24V from Battery</text>
        <x>41</x>
        <y>160</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPS24vOK</name>
        <pv_name>$(P):ElDiag_SplyFromPsu</pv_name>
        <x>15</x>
        <y>190</y>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPS24vOK</name>
        <text>UPS 24V from PSU</text>
        <x>41</x>
        <y>190</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSBuffReady</name>
        <pv_name>$(P):ElDiag_UpsRdyToBuff</pv_name>
        <x>15</x>
        <y>220</y>
        <off_color>
          <color name="LED-GREEN-OFF" red="90" green="110" blue="90">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSBuffReady</name>
        <text>UPS Ready to Buffer</text>
        <x>41</x>
        <y>220</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
      <widget type="led" version="2.0.0">
        <name>LED_UPSBatt85</name>
        <pv_name>$(P):ElDiag_UpsBatAbove85</pv_name>
        <x>15</x>
        <y>250</y>
        <off_color>
          <color name="LED-RED-ON" red="255" green="60" blue="46">
          </color>
        </off_color>
        <on_color>
          <color name="LED-GREEN-ON" red="70" green="255" blue="70">
          </color>
        </on_color>
      </widget>
      <widget type="label" version="2.0.0">
        <name>LB_UPSBatt85</name>
        <text>UPS Battery &gt; 85%</text>
        <x>41</x>
        <y>250</y>
        <width>170</width>
        <vertical_alignment>1</vertical_alignment>
      </widget>
    </widget>
  </widget>
</display>
